#   demo-pagination-api

_API DEMO de paginación en oracle con hibernate._

## Pre-requisitos

* OpenJDK 11+
* Maven 3.3+
* Oracle 12c


## Configuraciones

* Las configuraciones referente a la conexión con la base de datos se encuentran en el archivo application.properties

## Base de datos
Se puede utilizar la imagen docker [absolutapps/oracle-12c-ee](https://hub.docker.com/r/absolutapps/oracle-12c-ee)

* Instalar

```
docker pull absolutapps/oracle-12c-ee
```
* Iniciar  contenedor

```
docker run -d --name oracle-12c  --privileged -p 8080:8080 -p 1521:1521 absolutapps/oracle-12c-ee
```

### Construcci&oacute;n

```
mvn clean install

```

### Deploy

#### Maven


```
mvn spring-boot:run

```
Una vez desplegado acceder a http://localhost:8090

### Endpoints

Los enpoints disponibles para pruebas son :

* Paginado con hibernate

```
 curl -I  http://localhost:8090/employees?page=0&pageSize=5
```
* Paginado con query nativo

```
 curl -I http://localhost:8090/employees/withNativePagination?page=1&pageSize=10

```
page = número de página

pageSize = tamaño de página