package py.com.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import py.com.data.model.Employee;
import py.com.data.repository.EmployeeRepository;

@SpringBootApplication
public class DemoPaginationApiApplication implements CommandLineRunner{
	
	@Autowired
	private EmployeeRepository employeeRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoPaginationApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (employeeRepository.findAll().isEmpty()) {
			for (int i = 1; i < 1000; i++) {
				employeeRepository.save(new Employee("name " + i));
			}		
		}
	}

}
