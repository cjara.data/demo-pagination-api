package py.com.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import py.com.data.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query(value = "SELECT * From EMPLOYEES", countQuery = "SELECT count(*) From EMPLOYEES", nativeQuery = true)
	Page<Employee> findAllEmployeesWithPagination(Pageable pageable);

}
