package py.com.data.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.com.data.model.Employee;
import py.com.data.repository.EmployeeRepository;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
	
	 Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeRepository employeeRepository;

	@GetMapping
	public Page<Employee> getEmployees(@RequestParam("page") int page, @RequestParam("pageSize") int pageSize) {
		Pageable pageable = PageRequest.of(page, pageSize);
		logger.info("--> OFFSET :{}, NEXT: {}", pageable.getOffset(), pageable.getPageSize());
		return employeeRepository.findAll(pageable);
	}
	
	@GetMapping("/withNativePagination")
	public Page<Employee> getEmployeesWithNativePagination(@RequestParam("page") int page, @RequestParam("pageSize") int pageSize) {
		Pageable pageable = PageRequest.of(page, pageSize);
		logger.info("--> OFFSET :{}, NEXT: {}", pageable.getOffset(), pageable.getPageSize());
		return employeeRepository.findAllEmployeesWithPagination(pageable);
	}

}
